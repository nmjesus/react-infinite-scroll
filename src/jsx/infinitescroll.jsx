(function() {
  'use strict';

  function generateItems(n) {
    var i = 0;
    var arr = [];
    for(;i<n; i++) {
      arr.push({title: 'foo_' + Math.round(Math.random() * 99999)});
    }
    return arr;
  }

  var InfiniteScroll = React.createClass({
    getInitialState: function() {
      return {
        results: generateItems(10)
      }
    },

    _onClick: function() {
      this.setState({
        results: this.state.results.concat(generateItems(10))
      });
    },

    render: function() {
      return (
        <div>
          <ul>
            {this.state.results.map(function(el, idx) {
              return <li key={idx}>{el.title}</li>
            })}
          </ul>
          <button onClick={this._onClick}>Load more</button>
        </div>
      );
    }
  });

  ReactDOM.render(<InfiniteScroll />, document.getElementById('root'));
})();
