(function () {
  'use strict';

  function generateItems(n) {
    var i = 0;
    var arr = [];
    for (; i < n; i++) {
      arr.push({ title: 'foo_' + Math.round(Math.random() * 99999) });
    }
    return arr;
  }

  var InfiniteScroll = React.createClass({
    displayName: 'InfiniteScroll',

    getInitialState: function () {
      return {
        results: generateItems(10)
      };
    },

    _onClick: function () {
      this.setState({
        results: this.state.results.concat(generateItems(10))
      });
    },

    render: function () {
      return React.createElement(
        'div',
        null,
        React.createElement(
          'ul',
          null,
          this.state.results.map(function (el, idx) {
            return React.createElement(
              'li',
              { key: idx },
              el.title
            );
          })
        ),
        React.createElement(
          'button',
          { onClick: this._onClick },
          'Load more'
        )
      );
    }
  });

  ReactDOM.render(React.createElement(InfiniteScroll, null), document.getElementById('root'));
})();
